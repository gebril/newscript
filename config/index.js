export const I18N = {
  /** If true, vue-i18n-loader is added to Nuxt's Webpack config
   * (incase we want custom translations in components or anywhere
   * with <i18n> </i18n> tag. Set to false for now.
   */

  vueI18nLoader: false,
  locales: [
    {
      code: 'en',
      iso: 'en-US',
      name: 'English',
      file: 'en-US.js'
    },
    {
      code: 'ar',
      iso: 'ar-AE',
      name: 'عربى',
      file: 'ar-AE.js'
    }
  ],
  defaultLocale: 'en',
  //lazy loading messages this requires additional setup though maybe sprint 5 =).
  lazy: true,
  langDir: 'lang/',
  // Enable browser language detection to automatically redirect user
  // to their preferred language as they visit your app for the first time
  detectBrowserLanguage: {
    /** If enabled, a cookie is set once a user has been redirected to his
     *  preferred language to prevent subsequent redirections
     *  Set to false to redirect every time
     */

    useCookie: true,
    // Cookie name
    cookieKey: 'i18n_redirected',
    // Set to always redirect to value stored in the cookie, not just once
    // when we enable multi lang support this should be set to true
    alwaysRedirect: true,
    // If no locale for the browsers locale is a match, use this one as a fallback
    fallbackLocale: 'en'
  },

  // detectBrowserLanguage: false,
  /**
   * Set this to true if you're using different domains for each language
   * Please Note this will require ADDITIONAL SETUP if set to true so let me know
   * or check docs https://nuxt-community.github.io/nuxt-i18n/different-domains.html
   */
  differentDomains: false,
  /**
   * If true, SEO metadata is generated for routes that have i18n enabled
   *  Set to false to disable app-wide
   * set to true by default can be overwritten with head() in page components
   * docs say when true it can cause  performance issues so will disucess with team
   * what strategy to take
   */
  seo: true,
  /**
   *   Fallback base URL to use as prefix for alternate URLs in hreflang tags.
   * By default VueRouter's base URL will be used and only if that is not available,
   * fallback URL will be used.
   */
  baseUrl: '',
  /**
   * typescript decorators officially supported ..
   */
  parsePages: true,
  //This is where the messages are added! check lang folder for each lang translations!
  // vueI18n: {
  //   messages: { en, ar }
  // },
  // Called right before app's locale changes. Maybe we will need it
  beforeLanguageSwitch: (oldLocale, newLocale) => {
    //app.store.commit('web/SET_LOCALE', newLocale)
  },

  // Called after app's locale has changed. Maybe we will need it
  onLanguageSwitched: () => null
}
