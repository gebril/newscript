export default {
  routes: {
    home: "الصفحة الرئيسة",
    about_us: "عن Agreement",
    how_it_works: "كيف نعمل",
    who_we_are: "من نحن",
    contact_us: "تواصل معنا",
    customs_broker_list: "قائمة وسطاء التخليص الجمركي",
    register_as_broker: "سجل كوسيط تخليص جمركي",
    packages: "الباقات"
  },
  home_page: {
    business_opportunities: "فرص الأعمال",
    who_we_are: "من نحن",
    contact: "تواصل معنا",
    check_out_our_app: "قم بتحميل تطبيقنا"
  },
  contact_us: {
    contact_us: "تواصل معنا",
    full_name: "الاسم الكامل",
    email_address: "البريد الإلكتروني",
    number: "رقم الهاتف",
    subject: "العنوان",
    write_message: "اكتب رسالة",
    submit: "ارسل"
  },
  how_to_steps: {
    how_to_steps: "خطوات العمل"
  },
  customs_broker_list: {
    add_custom_brokers: "إضافة وسيط تخليص جمركي",
    email: "البريد الإلكتروني *",
    company_name: "اسم الشركة (باللغة الإنجليزية)*",
    company_name_ar: "اسم الشركة (باللغة العربية)*",
    phone: "الهاتف*",
    website: "الموقع*",
    address: "العنوان*",
    country: "الدولة*",
    city: "المدينة",
    logo: "الشعار",
    documents: "المستندات*",
    required_fields: "* تعني أن الحقل إجباري",
    close: "إغلاق",
    submit: "ارسال"
  },
  contact_custom_broker: {
    contact_request: "نموذج تواصل",
    name: "الاسم*",
    email: "البريد الإلكتروني*",
    phone: "الهاتف*",
    address: "العنوان*",
    company_registration: "سجل الشركة",
    preferred_contact: "جهة التواصل",
    shipment_details: "تفاصيل الشحنة*",
    payment_details: "تفاصيل الدفع*",
    required_fields: "* تعني أن الحقل إجباري",
    close: "اغلاق",
    submit: "ارسال"
  },
  rules: {
    email_required: "البريد الإلكتروني إجباري",
    email_invalid: "البريد الإلكتروني غير صحيح",
    max_seventy: "الحد الأقصى للحروف هو ٧٠ حرف فقط",
    required: "إجباري.",
    min_nine: "حد إدني ٩ أرقام",
    invalid_number: "الرقم غير صحيح."
  },
  success: "تم الإرسال بنجاح",
  failed: "فشلت محاولة الإرسال",
  issue: "يبدو أن هناك مشكلة في أحد الحقول!"
};
