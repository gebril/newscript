export default {
  routes: {
    home: "Home",
    about_us: "About Us",
    how_it_works: "How To Steps",
    who_we_are: "Who we are",
    contact_us: "Contact",
    customs_broker_list: "Customs Broker List",
    register_as_broker: "Register as Broker",
    packages: "Package"
  },
  home_page: {
    business_opportunities: "Business Opportunities",
    who_we_are: "WHO WE ARE",
    contact: "Contact",
    check_out_our_app: "Check out our app"
  },
  contact_us: {
    contact_us: "Contact Us",
    full_name: "Full Name",
    email_address: "Email Address",
    number: "Phone Number",
    subject: "Subject",
    write_message: "Write Message",
    submit: "Send"
  },
  how_to_steps: {
    how_to_steps: "HOW TO STEPS"
  },
  customs_broker_list: {
    add_custom_brokers: "Add Custom Broker",
    email: "Email*",
    company_name: "Company Name*",
    company_name_ar: "Company Name(Arabic)*",
    phone: "Phone*",
    website: "Website*",
    address: "Address*",
    country: "Country*",
    city: "City",
    logo: "Logo",
    documents: "Documents*",
    required_fields: "*indicates required field",
    close: "Close",
    submit: "Submit"
  },
  contact_custom_broker: {
    contact_request: "Contact Request",
    name: "Name*",
    email: "Email*",
    phone: "Phone*",
    address: "Address*",
    company_registration: "Company Registration",
    preferred_contact: "Preferred Contact",
    shipment_details: "Shipment Details*",
    payment_details: "Payment Details*",
    required_fields: "*indicates required field",
    close: "Close",
    submit: "Submit"
  },
  rules: {
    email_required: "E-mail is required",
    email_invalid: "Email is not valid",
    max_seventy: "Max 70 characters",
    required: "Required.",
    min_nine: "Min 9 characters",
    invalid_number: "Invalid Phone Number."
  },
  success: "Submitted successfully",
  failed: "Failed to Submit",
  issue: "There seems to be a issue with one of the fields!",
  packages: {
    duration: "Duration(Days)",
    price: "Price",
    features: "Features"
  }
};
