import "@mdi/font/css/materialdesignicons.css";

export default {
  customVariables: ["~/assets/style/_variables.scss"],
  theme: {
    options: {
      customProperties: true
    },
    dark: false,
    light: true,
    themes: {
      light: {
        primary: "#053476",
        secondary: "#3F6FC8",
        accent: "#F8DBB0",
        error: "#E50404",
        info: "#CD6632",
        success: "#4CAF50",
        warning: "#FFC107"
      }
    }
  },
  icons: {
    iconfont: "mdi"
  },
  font: {
    family: "Montserrat"
  },
  minifyTheme: function(css) {
    return process.env.NODE_ENV === "production"
      ? css.replace(/[\s|\r\n|\r|\n]/g, "")
      : css;
  }
};
