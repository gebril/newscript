import {Renovation} from "@leam-tech/renovation-core";

let renovation: Renovation | null = null;
let initialized = false;

export default async ({ env, store }) => {
  await init(env);
};

export async function init(env) {
  renovation = new Renovation();
  await renovation.init({
    backend: "frappe",
    hostURL: process.server ? env.serverBaseUrl : env.clientBaseUrl,
    clientId: env.clientId
  });
  console.log("Core Initialized");
  initialized = true;
}

export function hasInitialized() {
  return initialized;
}

export function getRenovationCore(): Renovation {
  if (!renovation) {
    throw new Error("Core not initialized; Please verify");
  }
  return renovation;
}
