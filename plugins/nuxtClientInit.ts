import { Plugin } from '@nuxt/types'

const nuxtClientInit: Plugin = async (context) => {
  await context.store.dispatch('nuxtClientInit', context)
}

export default nuxtClientInit
