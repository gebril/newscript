import { Store } from "vuex";
import { getModule } from "vuex-module-decorators";
import MessagesModule from "~/store/messages";
import WebsiteModule from "~/store/web";

let webStore: WebsiteModule;
let messagesStore: MessagesModule;

function initialiseStores(store: Store<any>): void {
  webStore = getModule(WebsiteModule, store);
  messagesStore = getModule(MessagesModule, store);
}

export { initialiseStores, webStore, messagesStore };
