export type HomePage = {
  title: string;
  content: string;
  main_image: string;
  cover_image: string;
  app_store: string;
  play_store: string;
};
export type AboutUsPage = {
  title: string;
  content: string;
  image: string;
};
export type HowItWorksPage = {
  title: string;
  description: string;
  steps: Array<{
    title: string;
    image: string;
    content: string;
  }>;
};
export type WhoWeArePage = {
  title: string;
  summary: string;
  content: string;
  image: string;
};
export type ContactUsPage = {
  title: string;
  image: string;
};

export type CustomBroker = {
  name: string;
  company_name: string;
  company_name_ar: string;
  website: string;
  address: Address | string;
  contact: string;
  email: string;
  logo: string;
  verified: boolean;
  documents: Array<Document>;
};

export type BusinessOpportunity = {
  title: string;
  description: string;
  image: string;
};

export type Package = {
  duration: number;
  enabled: boolean;
  image: string;
  price: number;
  title: string;
  title_ar: string;
  features: Array<Feature>;
};

export type Feature = {
  title: string;
  title_ar: string;
  description: string;
  description_ar: string;
  image: string;
};

export type CustomsBrokerContactRequest = {
  client_name: string;
  email: string;
  phone: string;
  address: string;
  company_registration: string;
  shipment_details: string;
  payment_details: string;
  preferred_contact: string;
  customs_broker_email: string;
};

export type Address = {
  address_line1: string;
  city: string;
  country: string;
  phone: string;
};

export type Document = {
  title: string;
  document: string | undefined;
};
