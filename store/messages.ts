import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";

import { Store } from "vuex";

export type Message = {
  type?: string;
  message?: string;
  data?: any;
};
// export type SnackbarMessage = {
//   type?: string;
//   message?: string;
// };
// export type DialogMessage = {
//   message?: string;
//   showControls?: string;
// };
@Module({
  namespaced: true,
  name: "messages",
  stateFactory: true,
  preserveState: false
})
export default class MessagesModule extends VuexModule {
  /*************************************************************** STATES ***************************************************************/

  /**
   * changes state if messages are present
   *
   * @private
   * @type {boolean}
   */
  private _isMessageShown: boolean = false;

  private _snackBarMessages: Message[] = [];
  private _snackbar: boolean = false;
  /**
   * List of messages
   *
   * @private
   * @type {Message[]}
   */
  private _messages: Message[] = [];
  /**
   * Getter for the "messages" state
   *
   * @returns {Message[]}
   */
  private _showControls: boolean | undefined = false;
  public get getShowControls() {
    return this._showControls;
  }
  public get getMessages(): Message[] {
    if (this._isMessageShown) {
      return this._messages;
    } else {
      return [];
    }
  }
  public get getSnackbarMessages() {
    return this._snackBarMessages;
  }
  /**
   * Getter for the "isMessageShown" state
   *
   * @returns {boolean}.
   */
  public get getIsMessageShown(): boolean {
    return this._isMessageShown;
  }

  public get getIsSnackBar() {
    return this._snackbar;
  }

  /**
   * Show a message
   *
   * @param message
   */
  @Action
  public showMessage({
    type,
    message,
    showControls
  }: {
    type?: string;
    message: string | Message;
    showControls?: boolean;
  }) {
    this.ADD_MESSAGE({ type, message, showControls });
    this.SHOW_MESSAGE(type);
  }
  /**
   * add a message
   *
   * Clear and Close Messages
   */
  @Action
  public clearAndClose() {
    this.HIDE_MESSAGE();
    this.CLEAR_MESSAGE();
  }
  @Action
  public clearAndCloseSnackBar() {
    this.CLEAR_SNACKBAR();
  }

  /**
   * Mutation to add a message
   *
   * @param message
   */
  @Mutation
  private ADD_MESSAGE({
    type,
    message,
    showControls
  }: {
    type?: string;
    message: string | Message;
    showControls?: boolean;
  }) {
    if (typeof message === "string") {
      message = {
        type: "Info",
        message: message
      };
    }
    if (type == "snackbar") {
      this._snackBarMessages.push(message);
      this._snackbar = true;
    } else {
      this._messages.push(message);
      this._showControls = showControls;
    }
  }
  /**
   * Mutation to show a message
   */
  @Mutation
  private SHOW_MESSAGE(type?: string) {
    if (!type || type !== "snackbar") {
      this._isMessageShown = true;
    } else {
      this._snackbar = true;
    }
  }
  /**
   * Mutation to hide a message
   */
  @Mutation
  private HIDE_MESSAGE() {
    this._isMessageShown = false;
  }
  /**
   * Mutation to clear a message
   */

  @Mutation
  private CLEAR_MESSAGE() {
    this._messages = [];
    this._showControls = false;
  }
  @Mutation
  private CLEAR_SNACKBAR() {
    this._snackbar = false;
    this._snackBarMessages = [];
  }
}
export function getMessagesStore(store?: Store<any>) {
  return getModule(MessagesModule, store);
}
