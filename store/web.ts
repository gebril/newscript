import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import { Store } from "vuex";
import {
  AboutUsPage,
  BusinessOpportunity,
  ContactUsPage,
  CustomBroker,
  HomePage,
  HowItWorksPage, Package,
  WhoWeArePage
} from "~/utils/type";
import { getRenovationCore } from "~/plugins/renovation.core";

@Module({
  namespaced: true,
  name: "web",
  stateFactory: true,
  preserveState: false
})
export default class WebsiteModule extends VuexModule {
  private _homePage: HomePage = {} as HomePage;
  private _aboutUsPage: AboutUsPage = {} as AboutUsPage;
  private _whoWeArePage: WhoWeArePage = {} as WhoWeArePage;
  private _howItWorksPage: HowItWorksPage = {} as HowItWorksPage;
  private _contactUsPage: ContactUsPage = {} as ContactUsPage;
  private _customBrokers: Array<CustomBroker> = [] as Array<CustomBroker>;
  private _businessOpportunities: Array<BusinessOpportunity> = [] as Array<
    BusinessOpportunity
  >;
  private _packages: Array<Package> = [] as Array<Package>;

  private locale: string = "";

  public get homePage(): HomePage {
    return this._homePage;
  }

  public get aboutUsPage(): AboutUsPage {
    return this._aboutUsPage;
  }

  public get howItWorksPage(): HowItWorksPage {
    return this._howItWorksPage;
  }

  public get whoWeArePage(): WhoWeArePage {
    return this._whoWeArePage;
  }

  public get contactUsPage(): ContactUsPage {
    return this._contactUsPage;
  }

  public get customBrokers(): Array<CustomBroker> {
    return this._customBrokers;
  }

  public get businessOpportunities(): Array<BusinessOpportunity> {
    return this._businessOpportunities;
  }

  public get packages(): Array<Package> {
    return this._packages;
  }

  public get getLocale() {
    return this.locale;
  }

  @Action
  public async loadHomepage() {
    let r = await getRenovationCore().call({
      cmd: "renovation_core.client.get",
      doctype: "Agreement Home Page",
      name: "Agreement Home Page",
      lang: this.locale,
    });
    if (r.success) {
      this.SET_HOME_PAGE(r.data.message as HomePage);
    }
  }

  @Action
  public async loadAboutUsPage() {
    let r = await getRenovationCore().call({
      cmd: "renovation_core.client.get",
      doctype: "Agreement About Us Page",
      name: "Agreement About Us Page",
      lang: this.locale,
    });
    if (r.success) {
      this.SET_ABOUT_US_PAGE(r.data.message as AboutUsPage);
    }
  }

  @Action
  public async loadWhoWeArePage() {
    let r = await getRenovationCore().call({
      cmd: "renovation_core.client.get",
      doctype: "Agreement Who We Are Page",
      name: "Agreement Who We Are Page",
      lang: this.locale,
    });
    if (r.success) {
      this.SET_WHO_WE_ARE_PAGE(r.data.message as WhoWeArePage);
    }
  }

  @Action
  public async loadHowItWorksPage() {
    let r = await getRenovationCore().call({
      cmd: "renovation_core.client.get",
      doctype: "Agreement How To Steps Page",
      name: "Agreement How To Steps Page",
      lang: this.locale,
    });
    if (r.success) {
      this.SET_HOW_IT_WORKS_PAGE(r.data.message as HowItWorksPage);
    }
  }

  @Action
  public async loadContactUsPage() {
    let r = await getRenovationCore().call({
      cmd: "renovation_core.client.get",
      doctype: "Agreement Contact Page",
      name: "Agreement Contact Page",
      lang: this.locale,
    });
    if (r.success) {
      this.SET_CONTACT_US_PAGE(r.data.message as ContactUsPage);
    }
  }

  @Action
  public async loadCustomsBroker() {
    let r = await getRenovationCore().call({
      cmd: "renovation_core.client.get_list",
      doctype: "Customs Broker",
      fields: ["*"],
      filters: JSON.stringify({
        verified: 1
      }),
      lang: this.locale,
    });
    if (r.success) {
      this.SET_CUSTOM_BROKER((r.data.message as unknown) as Array<CustomBroker>);
    }
  }

  @Action
  public async loadBusinessOpportunities() {
    let r = await getRenovationCore().call({
      cmd: "renovation_core.client.get_list",
      fields: ["*"],
      doctype: "Business Opportunity",
      lang: this.locale,
    });
    if (r.success) {
      this.SET_BUSINESS_OPPORTUNITY(
        (r.data.message as unknown) as Array<BusinessOpportunity>
      );
    }
  }

  @Action
  public async loadPackages() {
    let r = await getRenovationCore().model.getList({
      doctype: "Agreement Subscription",
      fields: ["*"],
      filters: {
        enabled: 1
      },
      tableFields: {
        features: ["*"]
      }
    });
    if (r.success) {
      this.SET_PACKAGES((r.data as unknown) as Array<Package>);
    }
  }

  @Mutation
  private SET_HOME_PAGE(home: HomePage) {
    this._homePage = home;
  }

  @Mutation
  private SET_ABOUT_US_PAGE(about_us: AboutUsPage) {
    this._aboutUsPage = about_us;
  }

  @Mutation
  private SET_WHO_WE_ARE_PAGE(who_we_are: WhoWeArePage) {
    this._whoWeArePage = who_we_are;
  }

  @Mutation
  private SET_HOW_IT_WORKS_PAGE(how_it_works: HowItWorksPage) {
    this._howItWorksPage = how_it_works;
  }

  @Mutation
  private SET_CONTACT_US_PAGE(contact_us: ContactUsPage) {
    this._contactUsPage = contact_us;
  }

  @Mutation
  private SET_CUSTOM_BROKER(custom_brokers: Array<CustomBroker>) {
    this._customBrokers = custom_brokers;
  }

  @Mutation
  private SET_BUSINESS_OPPORTUNITY(
    business_opportunities: Array<BusinessOpportunity>
  ) {
    this._businessOpportunities = business_opportunities;
  }

  @Mutation
  private SET_PACKAGES(packages: Array<Package>) {
    this._packages = packages;
  }

  @Mutation
  public SET_LOCALE(locale) {
    this.locale = locale;
  }
}

export function getWebsiteModule(store: Store<any>): WebsiteModule {
  return getModule(WebsiteModule, store);
}
