import { initialiseStores, webStore } from "~/utils/store-accessor";
import { Store } from "vuex";
const initializer = (store: Store<any>) => initialiseStores(store);
import { Context } from "@nuxt/types";
export const plugins = [initializer];
export * from "~/utils/store-accessor";

export const actions = {
  async nuxtServerInit({}, context: Context) {
    const start = new Date().getTime();
    // console.log("Initializing.....");
    initialiseStores(context.store);
    webStore.SET_LOCALE(context.store.$i18n.locale);
    // console.log("Initialized in: " + (new Date().getTime() - start));
  },
  async nuxtClientInit({ commit }, context: Context) {
    initialiseStores(context.store);
  }
};
