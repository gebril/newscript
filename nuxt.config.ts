import { I18N } from "./config";

export default {
  mode: "universal",
  /*
   ** Headers of the page
   */
  head: {
    // titleTemplate: '%s - ' + process.env.npm_package_name,
    title: "Agreement",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#053476" },
  // pageTransition: {
  //   name: 'fade',
  //   mode: 'out-in'
  // },
  /*
   ** Global CSS
   */
  css: ["~/assets/style/main.scss", "swiper/css/swiper.css"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/renovation.core.ts",
    { src: "~/plugins/nuxtClientInit", mode: "client" },
    { src: "~/plugins/swiper.js", mode: "client" }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxt/typescript-build", "@nuxtjs/vuetify"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxt/typescript-build",
    ["vue-scrollto/nuxt", { duration: 300 }],
    [
      "@nuxtjs/sitemap",
      {
        exclude: []
      }
    ],
    ["nuxt-i18n", I18N]
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    optionsPath: "~/plugins/vuetify.options.js"
  },
  /*
   ** Build configuration
   */
  build: {
    babel: {
      presets({ isServer }) {
        return [
          [
            require.resolve("@nuxt/babel-preset-app"),
            {
              buildTarget: isServer ? "server" : "client",
              corejs: { version: 3 }
            }
          ]
        ];
      },
      plugins: [
        ["@babel/plugin-proposal-decorators", { legacy: true }],
        ["@babel/plugin-proposal-class-properties", { loose: true }]
      ]
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // if (ctx && ctx.isClient) {
      //   config.optimization.splitChunks.maxSize = 51200;
      // }
    }
  },
  env: {
    serverBaseUrl: "https://backend-core.agrement.app",
    clientBaseUrl:
      process.env.NODE_ENV === "development"
        ? "https://backend-core.agrement.app"
        : "/api",
    clientId: "backend-core.agrement.app"
  }
};
